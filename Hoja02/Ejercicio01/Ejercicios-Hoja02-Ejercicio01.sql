﻿-- Ejercicio del Módulo 1 de Sistemas de Gestión de Información

-- Versión 0.1    2019-06-03    Solución inicial


/* Entidades: Clientes, Coches, Accidentes
 * Relaciones: poseer, sufrir
 *
 */



-- Borrar base de datos de forma reentrante. Funciona, pero requiere que tengas
-- permiso para crear bases de datos.
DROP DATABASE IF EXISTS `ejercicios_M1H2E1`;

CREATE DATABASE IF NOT EXISTS `ejercicios_M1H2E1` 
  CHARACTER SET utf8 
  COLLATE utf8_spanish_ci;

USE `ejercicios_M1H2E1`;


-- Entidades

 /* Entidad:  Clientes
  * Miembros: dni
  * Claves:   PRIMARY   dni
  *           -
  */
CREATE TABLE IF NOT EXISTS `Clientes` (
  `dni` varchar (9),
  PRIMARY KEY (dni)
  );


/* Entidad:   Coches
 * Miembros:  accidentes, matricula, año, modelo
 * Claves:    PRIMARY  matricula
 *
 */
CREATE TABLE IF NOT EXISTS `Coches` (
  `matricula` varchar(7),
  `accidentes` int(2),
  `año` year,
  `modelo` varchar(64),
  PRIMARY KEY (`matricula`)
  );


/* Entidad:   Accidentes
 * Miembros:  numero, fecha, situacion, pcDano
 * Claves:    PRIMARY   (`matricula`+`numero`)
 *
 */
CREATE TABLE IF NOT EXISTS `Accidentes` (
  `matricula` varchar(7),
  `numero` int,
  `fecha` date,
  `situacion` varchar(64),
  `pcDano` int(3),
  PRIMARY KEY (`matricula`,`numero`),
  CONSTRAINT `fk_matriculaCoche` FOREIGN KEY (`matricula`) REFERENCES `Coches`(`matricula`)
  );




 /* Relación: poseer (1:N)
  * Miembros: dni, matricula
  * Claves:   PRIMARY   dni, matricula
  *           UNIQUE    matricula
  *           FOREIGN   dniClientes -> Clientes(dni)
  *           FOREIGN   matriculaCoche -> Coches(matricula)
  *           -
  */
CREATE TABLE IF NOT EXISTS `poseer` (
  `dniCliente` varchar (9),
  `matriculaCoche`  varchar(7),
  PRIMARY KEY (`dniCliente`,`matriculaCoche`),
  CONSTRAINT `uk_posee_matriculaCoche` UNIQUE KEY (`matriculaCoche`),
  CONSTRAINT `fk_posee_dniCliente` FOREIGN KEY (`dniCliente`) REFERENCES `Clientes`(`dni`),
  CONSTRAINT `fk_posee_matriculaCoche` FOREIGN KEY  (`matriculaCoche`) REFERENCES `Coches`(`matricula`)
  );


/* Relación:  sufrir (1:N)
 * Miembros:  `matricula`, `numero`,
 * Claves:    PRIMARY   matricula` + `numero`
 *            FOREIGN   matricula -> Coches(matricula)
 *
 */
CREATE TABLE IF NOT EXISTS `sufrir` (
  `matricula` varchar(7),
  `numero` int,
  PRIMARY KEY (`matricula`, `numero`)/*,
  CONSTRAINT (`fk_sufre_matriculaCoche`) */,
  FOREIGN KEY (`matricula`)  REFERENCES `Coches`(`matricula`)
  );